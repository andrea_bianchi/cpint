module vrr_yz_8_0
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine vrr_yz_8_0_r(AUX_INT)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    616
!!
      implicit none
!
      real(8), dimension(*), intent(inout) :: AUX_INT
      integer(8) :: k_1
      integer(8) :: k_2
!
      do k_1 = 0_8, 7_8
         AUX_INT(((13922_8+(2_8+k_1))-1_8))=AUX_INT(((7407_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((13922_8+(1_8+k_2))-1_8))=AUX_INT(7407_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 7_8
         do k_2 = 0_8, 0_8
            AUX_INT(((13922_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13922_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((7407_8+(1_8+k_1))-1_8))* &
            AUX_INT(((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 6_8
         AUX_INT(((13931_8+(3_8+k_1))-1_8))=AUX_INT(((3604_8+(1_8+k_1))-1_8))*AUX_INT(40_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((13931_8+(1_8+k_2))-1_8))=AUX_INT(3604_8)*AUX_INT(((38_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 6_8
         do k_2 = 0_8, 1_8
            AUX_INT(((13931_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13931_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((3604_8+(1_8+k_1))-1_8))* &
            AUX_INT(((38_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((13940_8+(4_8+k_1))-1_8))=AUX_INT(((1556_8+(1_8+k_1))-1_8))*AUX_INT(178_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((13940_8+(1_8+k_2))-1_8))=AUX_INT(1556_8)*AUX_INT(((175_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 2_8
            AUX_INT(((13940_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13940_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1556_8+(1_8+k_1))-1_8))* &
            AUX_INT(((175_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((13949_8+(5_8+k_1))-1_8))=AUX_INT(((569_8+(1_8+k_1))-1_8))*AUX_INT(593_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((13949_8+(1_8+k_2))-1_8))=AUX_INT(569_8)*AUX_INT(((589_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 3_8
            AUX_INT(((13949_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13949_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((569_8+(1_8+k_1))-1_8))* &
            AUX_INT(((589_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((13958_8+(4_8+k_1))-1_8))=AUX_INT(((1586_8+(1_8+k_1))-1_8))*AUX_INT(166_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((13958_8+(1_8+k_2))-1_8))=AUX_INT(1586_8)*AUX_INT(((163_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 2_8
            AUX_INT(((13958_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13958_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1586_8+(1_8+k_1))-1_8))* &
            AUX_INT(((163_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 6_8
         AUX_INT(((13967_8+(3_8+k_1))-1_8))=AUX_INT(((3646_8+(1_8+k_1))-1_8))*AUX_INT(34_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((13967_8+(1_8+k_2))-1_8))=AUX_INT(3646_8)*AUX_INT(((32_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 6_8
         do k_2 = 0_8, 1_8
            AUX_INT(((13967_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13967_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((3646_8+(1_8+k_1))-1_8))* &
            AUX_INT(((32_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 7_8
         AUX_INT(((13976_8+(2_8+k_1))-1_8))=AUX_INT(((7463_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((13976_8+(1_8+k_2))-1_8))=AUX_INT(7463_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 7_8
         do k_2 = 0_8, 0_8
            AUX_INT(((13976_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((13976_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((7463_8+(1_8+k_1))-1_8))* &
            AUX_INT(((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 6_8
         AUX_INT(((7415_8+(2_8+k_1))-1_8))=AUX_INT(((3604_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((7415_8+(1_8+k_2))-1_8))=AUX_INT(3604_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 6_8
         do k_2 = 0_8, 0_8
            AUX_INT(((7415_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((7415_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((3604_8+(1_8+k_1))-1_8))* &
            AUX_INT(((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((7423_8+(3_8+k_1))-1_8))=AUX_INT(((1556_8+(1_8+k_1))-1_8))*AUX_INT(40_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((7423_8+(1_8+k_2))-1_8))=AUX_INT(1556_8)*AUX_INT(((38_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 1_8
            AUX_INT(((7423_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((7423_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1556_8+(1_8+k_1))-1_8))* &
            AUX_INT(((38_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((7431_8+(4_8+k_1))-1_8))=AUX_INT(((569_8+(1_8+k_1))-1_8))*AUX_INT(178_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((7431_8+(1_8+k_2))-1_8))=AUX_INT(569_8)*AUX_INT(((175_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 2_8
            AUX_INT(((7431_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((7431_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((569_8+(1_8+k_1))-1_8))* &
            AUX_INT(((175_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((7439_8+(4_8+k_1))-1_8))=AUX_INT(((589_8+(1_8+k_1))-1_8))*AUX_INT(166_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((7439_8+(1_8+k_2))-1_8))=AUX_INT(589_8)*AUX_INT(((163_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 2_8
            AUX_INT(((7439_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((7439_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((589_8+(1_8+k_1))-1_8))* &
            AUX_INT(((163_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((7447_8+(3_8+k_1))-1_8))=AUX_INT(((1586_8+(1_8+k_1))-1_8))*AUX_INT(34_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((7447_8+(1_8+k_2))-1_8))=AUX_INT(1586_8)*AUX_INT(((32_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 1_8
            AUX_INT(((7447_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((7447_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1586_8+(1_8+k_1))-1_8))* &
            AUX_INT(((32_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 6_8
         AUX_INT(((7455_8+(2_8+k_1))-1_8))=AUX_INT(((3646_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((7455_8+(1_8+k_2))-1_8))=AUX_INT(3646_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 6_8
         do k_2 = 0_8, 0_8
            AUX_INT(((7455_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((7455_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((3646_8+(1_8+k_1))-1_8))* &
            AUX_INT(((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((3611_8+(2_8+k_1))-1_8))=AUX_INT(((1556_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((3611_8+(1_8+k_2))-1_8))=AUX_INT(1556_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 0_8
            AUX_INT(((3611_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((3611_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1556_8+(1_8+k_1))-1_8))* &
            AUX_INT(((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((3618_8+(3_8+k_1))-1_8))=AUX_INT(((569_8+(1_8+k_1))-1_8))*AUX_INT(40_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((3618_8+(1_8+k_2))-1_8))=AUX_INT(569_8)*AUX_INT(((38_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 1_8
            AUX_INT(((3618_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((3618_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((569_8+(1_8+k_1))-1_8))* &
            AUX_INT(((38_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 3_8
         AUX_INT(((3625_8+(4_8+k_1))-1_8))=AUX_INT(((163_8+(1_8+k_1))-1_8))*AUX_INT(178_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((3625_8+(1_8+k_2))-1_8))=AUX_INT(163_8)*AUX_INT(((175_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 3_8
         do k_2 = 0_8, 2_8
            AUX_INT(((3625_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((3625_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((163_8+(1_8+k_1))-1_8))* &
            AUX_INT(((175_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((3632_8+(3_8+k_1))-1_8))=AUX_INT(((589_8+(1_8+k_1))-1_8))*AUX_INT(34_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((3632_8+(1_8+k_2))-1_8))=AUX_INT(589_8)*AUX_INT(((32_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 1_8
            AUX_INT(((3632_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((3632_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((589_8+(1_8+k_1))-1_8))* &
            AUX_INT(((32_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((3639_8+(2_8+k_1))-1_8))=AUX_INT(((1586_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((3639_8+(1_8+k_2))-1_8))=AUX_INT(1586_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 0_8
            AUX_INT(((3639_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((3639_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1586_8+(1_8+k_1))-1_8))* &
            AUX_INT(((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1562_8+(2_8+k_1))-1_8))=AUX_INT(((569_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((1562_8+(1_8+k_2))-1_8))=AUX_INT(569_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 0_8
            AUX_INT(((1562_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1562_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((569_8+(1_8+k_1))-1_8))* &
            AUX_INT(((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 3_8
         AUX_INT(((1568_8+(3_8+k_1))-1_8))=AUX_INT(((163_8+(1_8+k_1))-1_8))*AUX_INT(40_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((1568_8+(1_8+k_2))-1_8))=AUX_INT(163_8)*AUX_INT(((38_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 3_8
         do k_2 = 0_8, 1_8
            AUX_INT(((1568_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1568_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((163_8+(1_8+k_1))-1_8))* &
            AUX_INT(((38_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 3_8
         AUX_INT(((1574_8+(3_8+k_1))-1_8))=AUX_INT(((175_8+(1_8+k_1))-1_8))*AUX_INT(34_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((1574_8+(1_8+k_2))-1_8))=AUX_INT(175_8)*AUX_INT(((32_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 3_8
         do k_2 = 0_8, 1_8
            AUX_INT(((1574_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1574_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((175_8+(1_8+k_1))-1_8))* &
            AUX_INT(((32_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1580_8+(2_8+k_1))-1_8))=AUX_INT(((589_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((1580_8+(1_8+k_2))-1_8))=AUX_INT(589_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 0_8
            AUX_INT(((1580_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1580_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((589_8+(1_8+k_1))-1_8))* &
            AUX_INT(((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 3_8
         AUX_INT(((574_8+(2_8+k_1))-1_8))=AUX_INT(((163_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((574_8+(1_8+k_2))-1_8))=AUX_INT(163_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 3_8
         do k_2 = 0_8, 0_8
            AUX_INT(((574_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((574_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((163_8+(1_8+k_1))-1_8))*AUX_INT &
            (((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 2_8
         AUX_INT(((579_8+(3_8+k_1))-1_8))=AUX_INT(((32_8+(1_8+k_1))-1_8))*AUX_INT(40_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((579_8+(1_8+k_2))-1_8))=AUX_INT(32_8)*AUX_INT(((38_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 2_8
         do k_2 = 0_8, 1_8
            AUX_INT(((579_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((579_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((32_8+(1_8+k_1))-1_8))*AUX_INT( &
            ((38_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 3_8
         AUX_INT(((584_8+(2_8+k_1))-1_8))=AUX_INT(((175_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((584_8+(1_8+k_2))-1_8))=AUX_INT(175_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 3_8
         do k_2 = 0_8, 0_8
            AUX_INT(((584_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((584_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((175_8+(1_8+k_1))-1_8))*AUX_INT &
            (((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 2_8
         AUX_INT(((167_8+(2_8+k_1))-1_8))=AUX_INT(((32_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((167_8+(1_8+k_2))-1_8))=AUX_INT(32_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 2_8
         do k_2 = 0_8, 0_8
            AUX_INT(((167_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((167_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((32_8+(1_8+k_1))-1_8))*AUX_INT( &
            ((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 2_8
         AUX_INT(((171_8+(2_8+k_1))-1_8))=AUX_INT(((38_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((171_8+(1_8+k_2))-1_8))=AUX_INT(38_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 2_8
         do k_2 = 0_8, 0_8
            AUX_INT(((171_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((171_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((38_8+(1_8+k_1))-1_8))*AUX_INT( &
            ((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 1_8
         AUX_INT(((35_8+(2_8+k_1))-1_8))=AUX_INT(((4_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((35_8+(1_8+k_2))-1_8))=AUX_INT(4_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 1_8
         do k_2 = 0_8, 0_8
            AUX_INT(((35_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((35_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((4_8+(1_8+k_1))-1_8))*AUX_INT((( &
            6_8+(1_8+k_2))-1_8))
         enddo
      enddo
!
   end subroutine vrr_yz_8_0_r
!
end module vrr_yz_8_0

module primitive_loop_eri_0_4_module
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine primitive_loop_eri_0_4(g_shell,lA,n_primA,coeff_and_expA,centerA,is_pureA,lB,n_primB,coeff_and_expB,centerB,is_pureB &
   ,lC,n_primC,coeff_and_expC,centerC,is_pureC,lD,n_primD,coeff_and_expD,centerD,is_pureD,threshold,prefactor,skipped)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!   Calculates the eri integral with angular momentum 0 and 40 and 4
      use vrr_pre_intermediates_4_0, only: vrr_pre_intermediates_4_0_r
      use vrr_eri_explict_x_yz_1, only: vrr_eri_get_range_35_k_r
      use vrr_eri_explict_x_yz_1, only: vrr_eri_get_range_36_k_r
      use vrr_eri_explict_x_yz_1, only: vrr_eri_get_range_37_k_r
      use parameters_module, only: PURE_LENGTH
      use parameters_module, only: CART_LENGTH
      use boys_real_4, only: get_boys_4_real
!!
      implicit none
!
      real(8), dimension( *), intent(inout) :: g_shell
      integer(8), intent(in) :: lA
      integer(8), intent(in) :: n_primA
      real(8), dimension(2, n_primA), intent(in) :: coeff_and_expA
      real(8), dimension(3), intent(in) :: centerA
      logical(8), intent(in) :: is_pureA
      integer(8), intent(in) :: lB
      integer(8), intent(in) :: n_primB
      real(8), dimension(2, n_primB), intent(in) :: coeff_and_expB
      real(8), dimension(3), intent(in) :: centerB
      logical(8), intent(in) :: is_pureB
      integer(8), intent(in) :: lC
      integer(8), intent(in) :: n_primC
      real(8), dimension(2, n_primC), intent(in) :: coeff_and_expC
      real(8), dimension(3), intent(in) :: centerC
      logical(8), intent(in) :: is_pureC
      integer(8), intent(in) :: lD
      integer(8), intent(in) :: n_primD
      real(8), dimension(2, n_primD), intent(in) :: coeff_and_expD
      real(8), dimension(3), intent(in) :: centerD
      logical(8), intent(in) :: is_pureD
      real(8), intent(in) :: threshold
      real(8), intent(in) :: prefactor
      logical(8), intent(out) :: skipped
      real(8), dimension(5) :: boys_function
      real(8), dimension(27) :: in_vrr
      real(8), dimension(31) :: contracted_integral
      integer(8) :: max_l_bra
      integer(8) :: max_l_ket
      real(8) :: AB2
      real(8) :: CD2
      real(8) :: reference_value
      real(8) :: ESTIMATE_FACTOR
      integer(8) :: i_prim_C
      integer(8) :: i_prim_D
      integer(8) :: n_prim_CD
      real(8), dimension(6, n_primD*n_primC) :: pair_CD
      real(8) :: prefactor_AB
      integer(8) :: i_prim_A
      integer(8) :: i_prim_B
      real(8) :: final_prefactor
      real(8), dimension(31) :: uncontracted_integral
      real(8), dimension(593) :: auxiliary_integral_yz
      real(8), dimension(259) :: contracted_x_boys
      integer(8) :: dim_integral_AB
      integer(8) :: dim_integral_CD
!
      boys_function(1_8)=0.0_8
      contracted_integral=0.0_8
      if ((lA.ge.lB)) then
         max_l_bra=lA
         in_vrr(4_8:6_8)=centerA(:)
      else 
         max_l_bra=lB
         in_vrr(4_8:6_8)=centerB(:)
      endif 
      if ((lC.ge.lD)) then
         max_l_ket=lC
         in_vrr(1_8:3_8)=centerC(:)
      else 
         max_l_ket=lD
         in_vrr(1_8:3_8)=centerD(:)
      endif 
      AB2=(centerA(1_8)-centerB(1_8))**2_8
      AB2=AB2+(centerA(2_8)-centerB(2_8))**2_8
      AB2=AB2+(centerA(3_8)-centerB(3_8))**2_8
      CD2=(centerC(1_8)-centerD(1_8))**2_8
      CD2=CD2+(centerC(2_8)-centerD(2_8))**2_8
      CD2=CD2+(centerC(3_8)-centerD(3_8))**2_8
      reference_value=threshold
      ESTIMATE_FACTOR=(sqrt(CD2)+1.0_8)**4_8
      n_prim_CD=1_8
      do i_prim_C = 1_8, n_primC
         do i_prim_D = 1_8, n_primD
            pair_CD(1_8,n_prim_CD)=coeff_and_expC(2_8,i_prim_C)+coeff_and_expD(2_8,i_prim_D)
            pair_CD(2_8,n_prim_CD)=1.0_8/pair_CD(1_8,n_prim_CD)
            pair_CD(3_8,n_prim_CD)=5.568327996831708_8*coeff_and_expC(1_8,i_prim_C)*coeff_and_expD(1_8,i_prim_D)*pair_CD(2_8, &
            n_prim_CD)*exp(-coeff_and_expC(2_8,i_prim_C)*coeff_and_expD(2_8,i_prim_D)*pair_CD(2_8,n_prim_CD)*CD2)
            prefactor_AB=6.283185307179586_8*prefactor*pair_CD(3_8,n_prim_CD)
            if ((abs(prefactor_AB)*ESTIMATE_FACTOR.le.threshold*reference_value)) then
               cycle
            endif 
            pair_CD(4_8:6_8,n_prim_CD)=(coeff_and_expC(2_8,i_prim_C)*centerC(:)+coeff_and_expD(2_8,i_prim_D)*centerD(:))*pair_CD( &
            2_8,n_prim_CD)
            n_prim_CD=n_prim_CD+1_8
         enddo
      enddo
      n_prim_CD=n_prim_CD-1_8
      do i_prim_A = 1_8, n_primA
         do i_prim_B = 1_8, n_primB
            in_vrr(9_8)=coeff_and_expA(2_8,i_prim_A)+coeff_and_expB(2_8,i_prim_B)
            in_vrr(10_8)=1.0_8/in_vrr(9_8)
            in_vrr(15_8:17_8)=(coeff_and_expA(2_8,i_prim_A)*centerA(:)+coeff_and_expB(2_8,i_prim_B)*centerB(:))*in_vrr(10_8)
            prefactor_AB=6.283185307179586_8*prefactor*coeff_and_expA(1_8,i_prim_A)*coeff_and_expB(1_8,i_prim_B)*in_vrr(10_8)*exp( &
            -coeff_and_expA(2_8,i_prim_A)*coeff_and_expB(2_8,i_prim_B)*in_vrr(10_8)*AB2)
            if ((abs(prefactor_AB)*ESTIMATE_FACTOR.le.threshold*reference_value)) then
               cycle
            endif 
            do i_prim_C = 1_8, n_prim_CD
               in_vrr(7_8)=pair_CD(1_8,i_prim_C)
               in_vrr(8_8)=pair_CD(2_8,i_prim_C)
               in_vrr(11_8)=1.0_8/(in_vrr(7_8)+in_vrr(9_8))
               final_prefactor=prefactor_AB*sqrt(in_vrr(11_8))*pair_CD(3_8,i_prim_C)
               if ((abs(final_prefactor)*ESTIMATE_FACTOR.le.threshold*reference_value)) then
                  cycle
               endif 
               reference_value=max(abs(prefactor_AB),reference_value)
               in_vrr(12_8:14_8)=pair_CD(4_8:6_8,i_prim_C)
               in_vrr(18_8:20_8)=in_vrr(12_8:14_8)-in_vrr(15_8:17_8)
               call get_boys_4_real(boys_function,in_vrr(7_8)*in_vrr(9_8)*in_vrr(11_8)*((in_vrr(18_8)**2_8+in_vrr(19_8)**2_8)+ &
               in_vrr(20_8)**2_8))
               boys_function=final_prefactor*boys_function
               call vrr_pre_intermediates_4_0_r(boys_function,in_vrr,auxiliary_integral_yz,contracted_x_boys)
               select case (lA)
                  case (0_8)
                     select case (lC)
                        case (0_8)
                           call vrr_eri_get_range_35_k_r(boys_function,contracted_x_boys,auxiliary_integral_yz, &
                           uncontracted_integral)
                           contracted_integral(1_8:15_8)=contracted_integral(1_8:15_8)+1.0_8*uncontracted_integral(1_8:15_8)
                        case (1_8)
                           call vrr_eri_get_range_36_k_r(boys_function,contracted_x_boys,auxiliary_integral_yz, &
                           uncontracted_integral)
                           contracted_integral(1_8:10_8)=contracted_integral(1_8:10_8)+1.0_8*uncontracted_integral(1_8:10_8)
                           contracted_integral(11_8:25_8)=contracted_integral(11_8:25_8)+1.0_8*uncontracted_integral(11_8:25_8)
                        case (2_8)
                           call vrr_eri_get_range_37_k_r(boys_function,contracted_x_boys,auxiliary_integral_yz, &
                           uncontracted_integral)
                           contracted_integral(1_8:6_8)=contracted_integral(1_8:6_8)+1.0_8*uncontracted_integral(1_8:6_8)
                           contracted_integral(7_8:16_8)=contracted_integral(7_8:16_8)+1.0_8*uncontracted_integral(7_8:16_8)
                           contracted_integral(17_8:31_8)=contracted_integral(17_8:31_8)+1.0_8*uncontracted_integral(17_8:31_8)
                        case (3_8)
                           call vrr_eri_get_range_36_k_r(boys_function,contracted_x_boys,auxiliary_integral_yz, &
                           uncontracted_integral)
                           contracted_integral(1_8:10_8)=contracted_integral(1_8:10_8)+1.0_8*uncontracted_integral(1_8:10_8)
                           contracted_integral(11_8:25_8)=contracted_integral(11_8:25_8)+1.0_8*uncontracted_integral(11_8:25_8)
                        case (4_8)
                           call vrr_eri_get_range_35_k_r(boys_function,contracted_x_boys,auxiliary_integral_yz, &
                           uncontracted_integral)
                           contracted_integral(1_8:15_8)=contracted_integral(1_8:15_8)+1.0_8*uncontracted_integral(1_8:15_8)
                     end select
               end select
            enddo
         enddo
      enddo
      if ((boys_function(1_8).eq.0.0_8)) then
         skipped=.True._8
         if (is_pureA) then
            dim_integral_AB=PURE_LENGTH(lA)
         else 
            dim_integral_AB=CART_LENGTH(lA)
         endif 
         if (is_pureB) then
            dim_integral_AB=dim_integral_AB*PURE_LENGTH(lB)
         else 
            dim_integral_AB=dim_integral_AB*CART_LENGTH(lB)
         endif 
         if (is_pureC) then
            dim_integral_CD=PURE_LENGTH(lC)
         else 
            dim_integral_CD=CART_LENGTH(lC)
         endif 
         if (is_pureD) then
            dim_integral_CD=dim_integral_CD*PURE_LENGTH(lD)
         else 
            dim_integral_CD=dim_integral_CD*CART_LENGTH(lD)
         endif 
         g_shell(1_8:dim_integral_AB*dim_integral_CD*1_8)=0.0_8
      else 
         skipped=.False._8
         if (is_pureA) then
            select case (lA)
               case (0_8)
                  select case (lC)
                     case (0_8)
                        call primitive_loop_eri_0p_0p_0p_4p_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (1_8)
                        call primitive_loop_eri_0p_0p_1p_3p_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (2_8)
                        call primitive_loop_eri_0p_0p_2p_2p_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (3_8)
                        call primitive_loop_eri_0p_0p_3p_1p_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (4_8)
                        call primitive_loop_eri_0p_0p_4p_0p_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                  end select
            end select
         else 
            select case (lA)
               case (0_8)
                  select case (lC)
                     case (0_8)
                        call primitive_loop_eri_0c_0c_0c_4c_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (1_8)
                        call primitive_loop_eri_0c_0c_1c_3c_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (2_8)
                        call primitive_loop_eri_0c_0c_2c_2c_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (3_8)
                        call primitive_loop_eri_0c_0c_3c_1c_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                     case (4_8)
                        call primitive_loop_eri_0c_0c_4c_0c_k(g_shell,contracted_integral,centerA,centerB,centerC,centerD)
                  end select
            end select
         endif 
      endif 
!
   end subroutine primitive_loop_eri_0_4
   subroutine primitive_loop_eri_0p_0p_0p_4p_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_0_4, only: HRR_wrapper_eri_1_0_4_A_pp_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(15) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_0_4_A_pp_final_transposed(1_8,HRR_INTEGRAL(1_8:15_8),G_SHELL(1_8:9_8),centerC,centerD)
!
   end subroutine primitive_loop_eri_0p_0p_0p_4p_k
   subroutine primitive_loop_eri_0p_0p_1p_3p_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_1_3, only: HRR_wrapper_eri_1_1_3_A_pp_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(25) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (10_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*10_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((11_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((11_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_1_3_A_pp_final_transposed(1_8,HRR_INTEGRAL(1_8:10_8),HRR_INTEGRAL(11_8:25_8),G_SHELL(1_8:21_8), &
      centerC,centerD)
!
   end subroutine primitive_loop_eri_0p_0p_1p_3p_k
   subroutine primitive_loop_eri_0p_0p_2p_2p_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_2_2, only: HRR_wrapper_eri_1_2_2_B_pp_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(31) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (6_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*6_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (10_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((7_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((7_8+(((0_8-1_8)+J)*10_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((17_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((17_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_2_2_B_pp_final_transposed(1_8,HRR_INTEGRAL(1_8:6_8),HRR_INTEGRAL(7_8:16_8),HRR_INTEGRAL(17_8:31_8), &
      G_SHELL(1_8:25_8),centerC,centerD)
!
   end subroutine primitive_loop_eri_0p_0p_2p_2p_k
   subroutine primitive_loop_eri_0p_0p_3p_1p_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_3_1, only: HRR_wrapper_eri_1_3_1_B_pp_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(25) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (10_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*10_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((11_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((11_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_3_1_B_pp_final_transposed(1_8,HRR_INTEGRAL(1_8:10_8),HRR_INTEGRAL(11_8:25_8),G_SHELL(1_8:21_8), &
      centerC,centerD)
!
   end subroutine primitive_loop_eri_0p_0p_3p_1p_k
   subroutine primitive_loop_eri_0p_0p_4p_0p_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_4_0, only: HRR_wrapper_eri_1_4_0_B_pp_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(15) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_4_0_B_pp_final_transposed(1_8,HRR_INTEGRAL(1_8:15_8),G_SHELL(1_8:9_8),centerC,centerD)
!
   end subroutine primitive_loop_eri_0p_0p_4p_0p_k
   subroutine primitive_loop_eri_0c_0c_0c_4c_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_0_4, only: HRR_wrapper_eri_1_0_4_A_cc_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(15) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_0_4_A_cc_final_transposed(1_8,HRR_INTEGRAL(1_8:15_8),G_SHELL(1_8:15_8),centerC,centerD)
!
   end subroutine primitive_loop_eri_0c_0c_0c_4c_k
   subroutine primitive_loop_eri_0c_0c_1c_3c_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_1_3, only: HRR_wrapper_eri_1_1_3_A_cc_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(25) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (10_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*10_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((11_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((11_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_1_3_A_cc_final_transposed(1_8,HRR_INTEGRAL(1_8:10_8),HRR_INTEGRAL(11_8:25_8),G_SHELL(1_8:30_8), &
      centerC,centerD)
!
   end subroutine primitive_loop_eri_0c_0c_1c_3c_k
   subroutine primitive_loop_eri_0c_0c_2c_2c_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_2_2, only: HRR_wrapper_eri_1_2_2_B_cc_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(31) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (6_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*6_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (10_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((7_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((7_8+(((0_8-1_8)+J)*10_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((17_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((17_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_2_2_B_cc_final_transposed(1_8,HRR_INTEGRAL(1_8:6_8),HRR_INTEGRAL(7_8:16_8),HRR_INTEGRAL(17_8:31_8), &
      G_SHELL(1_8:36_8),centerC,centerD)
!
   end subroutine primitive_loop_eri_0c_0c_2c_2c_k
   subroutine primitive_loop_eri_0c_0c_3c_1c_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_3_1, only: HRR_wrapper_eri_1_3_1_B_cc_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(25) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (10_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*10_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((11_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((11_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_3_1_B_cc_final_transposed(1_8,HRR_INTEGRAL(1_8:10_8),HRR_INTEGRAL(11_8:25_8),G_SHELL(1_8:30_8), &
      centerC,centerD)
!
   end subroutine primitive_loop_eri_0c_0c_3c_1c_k
   subroutine primitive_loop_eri_0c_0c_4c_0c_k(G_SHELL,contracted_integral,centerA,centerB,centerC,centerD)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use HRR_wrapper_overlap_0_4_0, only: HRR_wrapper_eri_1_4_0_B_cc_final_transposed
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: G_SHELL
      real(8), dimension(31), intent(in) :: contracted_integral
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      real(8), dimension(3), intent(in) :: centerC
      real(8), dimension(3), intent(in) :: centerD
      real(8), dimension(15) :: HRR_INTEGRAL
      integer(8) :: I
      real(8), dimension(1) :: HALF_TRANS
      real(8), dimension(3) :: AB_CD
      real(8), dimension(1) :: CART_TRANS
      integer(8) :: J
!
      do I = 0_8, (15_8-1_8)
         CART_TRANS(1_8)=contracted_integral(((1_8+((0_8+1_8)+I))-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*1_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*1_8))
         enddo
         do J = 1_8, 1_8
            HRR_INTEGRAL(((1_8+(((0_8-1_8)+J)*15_8+(I+1_8)))-1_8))=1.0_8*1.0_8*HALF_TRANS((0_8+J))
         enddo
      enddo
      call HRR_wrapper_eri_1_4_0_B_cc_final_transposed(1_8,HRR_INTEGRAL(1_8:15_8),G_SHELL(1_8:15_8),centerC,centerD)
!
   end subroutine primitive_loop_eri_0c_0c_4c_0c_k
!
end module primitive_loop_eri_0_4_module

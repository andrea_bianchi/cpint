module HRR_wrapper_quadrupole_6_4_0_3_0
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine HRR_wrapper_quadrupole_6_4_0_3_0_B_pp_final(dim,a_in_3_0_0,a_in_4_0_0,a_in_5_0_0,a_out,centerA,centerB)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    692
      use hrr_cart_3_2_B, only: hrr_cart_3_2_B_r
      use hrr_cart_3_1_B, only: hrr_cart_3_1_B_r
!!
      implicit none
!
      integer(8), intent(in) :: dim
      real(8), dimension(10_8*dim), intent(in) :: a_in_3_0_0
      real(8), dimension(15_8*dim), intent(in) :: a_in_4_0_0
      real(8), dimension(21_8*dim), intent(in) :: a_in_5_0_0
      real(8), dimension(*), intent(out) :: a_out
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      integer(8) :: I
      real(8), dimension(9) :: AB
      real(8), dimension(10) :: HALF_TRANS
      real(8), dimension(60) :: CART_TRANS
      integer(8) :: J
!
      AB(1_8:3_8)=centerA-centerB
      AB(4_8)=AB(3_8)*AB(1_8)
      AB(5_8)=AB(2_8)*AB(1_8)
      AB(6_8)=AB(1_8)*AB(1_8)
      AB(7_8)=AB(3_8)*AB(2_8)
      AB(8_8)=AB(2_8)*AB(2_8)
      AB(9_8)=AB(3_8)*AB(3_8)
      do I = 0_8, (dim-1_8)
         call hrr_cart_3_2_B_r(AB,CART_TRANS,a_in_3_0_0((1_8+10_8*I):10_8*(I+1_8)),a_in_4_0_0((1_8+15_8*I):15_8*(I+1_8)), &
         a_in_5_0_0((1_8+21_8*I):21_8*(I+1_8)))
         do J = 1_8, 10_8
            HALF_TRANS((0_8+J))=1.0_8*1.0_8*CART_TRANS((4_8*10_8+J))
         enddo
         do J = 0_8, (1_8-1_8)
            a_out(((1_8+J*7_8)+I*7_8*1_8))=1.0_8*2.371708245126285_8*HALF_TRANS((2_8+J*10_8))+1.0_8*(-0.790569415042095_8)* &
            HALF_TRANS((7_8+J*10_8))
            a_out(((2_8+J*7_8)+I*7_8*1_8))=1.0_8*3.872983346207418_8*HALF_TRANS((5_8+J*10_8))
            a_out(((3_8+J*7_8)+I*7_8*1_8))=(1.0_8*(-0.612372435695795_8)*HALF_TRANS((2_8+J*10_8))+1.0_8*(-0.612372435695795_8)* &
            HALF_TRANS((7_8+J*10_8)))+1.0_8*2.449489742783178_8*HALF_TRANS((9_8+J*10_8))
            a_out(((4_8+J*7_8)+I*7_8*1_8))=(1.0_8*(-1.5_8)*HALF_TRANS((3_8+J*10_8))+1.0_8*(-1.5_8)*HALF_TRANS((8_8+J*10_8)))+1.0_8 &
            *1.0_8*HALF_TRANS((10_8+J*10_8))
            a_out(((5_8+J*7_8)+I*7_8*1_8))=(1.0_8*(-0.612372435695795_8)*HALF_TRANS((1_8+J*10_8))+1.0_8*(-0.612372435695795_8)* &
            HALF_TRANS((4_8+J*10_8)))+1.0_8*2.449489742783178_8*HALF_TRANS((6_8+J*10_8))
            a_out(((6_8+J*7_8)+I*7_8*1_8))=1.0_8*1.936491673103709_8*HALF_TRANS((3_8+J*10_8))+1.0_8*(-1.936491673103709_8)* &
            HALF_TRANS((8_8+J*10_8))
            a_out(((7_8+J*7_8)+I*7_8*1_8))=1.0_8*0.790569415042095_8*HALF_TRANS((1_8+J*10_8))+1.0_8*(-2.371708245126285_8)* &
            HALF_TRANS((4_8+J*10_8))
         enddo
         call hrr_cart_3_1_B_r(AB,CART_TRANS,a_in_3_0_0((1_8+10_8*I):10_8*(I+1_8)),a_in_4_0_0((1_8+15_8*I):15_8*(I+1_8)))
         do J = 1_8, 10_8
            HALF_TRANS((0_8+J))=1.0_8*1.0_8*1.0_8*centerB(3_8)*CART_TRANS((1_8*10_8+J))+1.0_8*1.0_8*1.0_8*centerB(2_8)*CART_TRANS( &
            (2_8*10_8+J))
         enddo
         do J = 0_8, (1_8-1_8)
            a_out(((1_8+J*7_8)+I*7_8*1_8))=a_out(((1_8+J*7_8)+I*7_8*1_8))+(1.0_8*2.371708245126285_8*HALF_TRANS((2_8+J*10_8))+ &
            1.0_8*(-0.790569415042095_8)*HALF_TRANS((7_8+J*10_8)))
            a_out(((2_8+J*7_8)+I*7_8*1_8))=a_out(((2_8+J*7_8)+I*7_8*1_8))+1.0_8*3.872983346207418_8*HALF_TRANS((5_8+J*10_8))
            a_out(((3_8+J*7_8)+I*7_8*1_8))=a_out(((3_8+J*7_8)+I*7_8*1_8))+((1.0_8*(-0.612372435695795_8)*HALF_TRANS((2_8+J*10_8))+ &
            1.0_8*(-0.612372435695795_8)*HALF_TRANS((7_8+J*10_8)))+1.0_8*2.449489742783178_8*HALF_TRANS((9_8+J*10_8)))
            a_out(((4_8+J*7_8)+I*7_8*1_8))=a_out(((4_8+J*7_8)+I*7_8*1_8))+((1.0_8*(-1.5_8)*HALF_TRANS((3_8+J*10_8))+1.0_8*(-1.5_8) &
            *HALF_TRANS((8_8+J*10_8)))+1.0_8*1.0_8*HALF_TRANS((10_8+J*10_8)))
            a_out(((5_8+J*7_8)+I*7_8*1_8))=a_out(((5_8+J*7_8)+I*7_8*1_8))+((1.0_8*(-0.612372435695795_8)*HALF_TRANS((1_8+J*10_8))+ &
            1.0_8*(-0.612372435695795_8)*HALF_TRANS((4_8+J*10_8)))+1.0_8*2.449489742783178_8*HALF_TRANS((6_8+J*10_8)))
            a_out(((6_8+J*7_8)+I*7_8*1_8))=a_out(((6_8+J*7_8)+I*7_8*1_8))+(1.0_8*1.936491673103709_8*HALF_TRANS((3_8+J*10_8))+ &
            1.0_8*(-1.936491673103709_8)*HALF_TRANS((8_8+J*10_8)))
            a_out(((7_8+J*7_8)+I*7_8*1_8))=a_out(((7_8+J*7_8)+I*7_8*1_8))+(1.0_8*0.790569415042095_8*HALF_TRANS((1_8+J*10_8))+ &
            1.0_8*(-2.371708245126285_8)*HALF_TRANS((4_8+J*10_8)))
         enddo
         CART_TRANS(1_8)=a_in_3_0_0((((1_8+10_8*I)+1_8)-1_8))*1.0_8
         CART_TRANS(2_8)=a_in_3_0_0((((1_8+10_8*I)+2_8)-1_8))*1.0_8
         CART_TRANS(3_8)=a_in_3_0_0((((1_8+10_8*I)+3_8)-1_8))*1.0_8
         CART_TRANS(4_8)=a_in_3_0_0((((1_8+10_8*I)+4_8)-1_8))*1.0_8
         CART_TRANS(5_8)=a_in_3_0_0((((1_8+10_8*I)+5_8)-1_8))*1.0_8
         CART_TRANS(6_8)=a_in_3_0_0((((1_8+10_8*I)+6_8)-1_8))*1.0_8
         CART_TRANS(7_8)=a_in_3_0_0((((1_8+10_8*I)+7_8)-1_8))*1.0_8
         CART_TRANS(8_8)=a_in_3_0_0((((1_8+10_8*I)+8_8)-1_8))*1.0_8
         CART_TRANS(9_8)=a_in_3_0_0((((1_8+10_8*I)+9_8)-1_8))*1.0_8
         CART_TRANS(10_8)=a_in_3_0_0((((1_8+10_8*I)+10_8)-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*7_8))=1.0_8*2.371708245126285_8*CART_TRANS((2_8+J*10_8))+1.0_8*(-0.790569415042095_8)*CART_TRANS(( &
            7_8+J*10_8))
            HALF_TRANS((2_8+J*7_8))=1.0_8*3.872983346207418_8*CART_TRANS((5_8+J*10_8))
            HALF_TRANS((3_8+J*7_8))=(1.0_8*(-0.612372435695795_8)*CART_TRANS((2_8+J*10_8))+1.0_8*(-0.612372435695795_8)*CART_TRANS &
            ((7_8+J*10_8)))+1.0_8*2.449489742783178_8*CART_TRANS((9_8+J*10_8))
            HALF_TRANS((4_8+J*7_8))=(1.0_8*(-1.5_8)*CART_TRANS((3_8+J*10_8))+1.0_8*(-1.5_8)*CART_TRANS((8_8+J*10_8)))+1.0_8*1.0_8* &
            CART_TRANS((10_8+J*10_8))
            HALF_TRANS((5_8+J*7_8))=(1.0_8*(-0.612372435695795_8)*CART_TRANS((1_8+J*10_8))+1.0_8*(-0.612372435695795_8)*CART_TRANS &
            ((4_8+J*10_8)))+1.0_8*2.449489742783178_8*CART_TRANS((6_8+J*10_8))
            HALF_TRANS((6_8+J*7_8))=1.0_8*1.936491673103709_8*CART_TRANS((3_8+J*10_8))+1.0_8*(-1.936491673103709_8)*CART_TRANS(( &
            8_8+J*10_8))
            HALF_TRANS((7_8+J*7_8))=1.0_8*0.790569415042095_8*CART_TRANS((1_8+J*10_8))+1.0_8*(-2.371708245126285_8)*CART_TRANS(( &
            4_8+J*10_8))
         enddo
         do J = 1_8, 7_8
            a_out(((0_8+J)+I*1_8*7_8))=a_out(((0_8+J)+I*1_8*7_8))+1.0_8*1.0_8*1.0_8*centerB(2_8)*centerB(3_8)*HALF_TRANS((0_8+J))
         enddo
      enddo
!
   end subroutine HRR_wrapper_quadrupole_6_4_0_3_0_B_pp_final
   subroutine HRR_wrapper_quadrupole_6_4_0_3_0_B_cc_final(dim,a_in_3_0_0,a_in_4_0_0,a_in_5_0_0,a_out,centerA,centerB)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    629
      use hrr_cart_3_2_B, only: hrr_cart_3_2_B_r
      use hrr_cart_3_1_B, only: hrr_cart_3_1_B_r
!!
      implicit none
!
      integer(8), intent(in) :: dim
      real(8), dimension(10_8*dim), intent(in) :: a_in_3_0_0
      real(8), dimension(15_8*dim), intent(in) :: a_in_4_0_0
      real(8), dimension(21_8*dim), intent(in) :: a_in_5_0_0
      real(8), dimension(*), intent(out) :: a_out
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      integer(8) :: I
      real(8), dimension(9) :: AB
      real(8), dimension(10) :: HALF_TRANS
      real(8), dimension(60) :: CART_TRANS
      integer(8) :: J
!
      AB(1_8:3_8)=centerA-centerB
      AB(4_8)=AB(3_8)*AB(1_8)
      AB(5_8)=AB(2_8)*AB(1_8)
      AB(6_8)=AB(1_8)*AB(1_8)
      AB(7_8)=AB(3_8)*AB(2_8)
      AB(8_8)=AB(2_8)*AB(2_8)
      AB(9_8)=AB(3_8)*AB(3_8)
      do I = 0_8, (dim-1_8)
         call hrr_cart_3_2_B_r(AB,CART_TRANS,a_in_3_0_0((1_8+10_8*I):10_8*(I+1_8)),a_in_4_0_0((1_8+15_8*I):15_8*(I+1_8)), &
         a_in_5_0_0((1_8+21_8*I):21_8*(I+1_8)))
         do J = 1_8, 10_8
            HALF_TRANS((0_8+J))=1.0_8*1.0_8*CART_TRANS((4_8*10_8+J))
         enddo
         do J = 0_8, (1_8-1_8)
            a_out(((1_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((1_8+J*10_8))
            a_out(((2_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((2_8+J*10_8))
            a_out(((3_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((3_8+J*10_8))
            a_out(((4_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((4_8+J*10_8))
            a_out(((5_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((5_8+J*10_8))
            a_out(((6_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((6_8+J*10_8))
            a_out(((7_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((7_8+J*10_8))
            a_out(((8_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((8_8+J*10_8))
            a_out(((9_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((9_8+J*10_8))
            a_out(((10_8+J*10_8)+I*10_8*1_8))=1.0_8*1.0_8*HALF_TRANS((10_8+J*10_8))
         enddo
         call hrr_cart_3_1_B_r(AB,CART_TRANS,a_in_3_0_0((1_8+10_8*I):10_8*(I+1_8)),a_in_4_0_0((1_8+15_8*I):15_8*(I+1_8)))
         do J = 1_8, 10_8
            HALF_TRANS((0_8+J))=1.0_8*1.0_8*centerB(3_8)*CART_TRANS((1_8*10_8+J))+1.0_8*1.0_8*centerB(2_8)*CART_TRANS((2_8*10_8+J) &
            )
         enddo
         do J = 0_8, (1_8-1_8)
            a_out(((1_8+J*10_8)+I*10_8*1_8))=a_out(((1_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((1_8+J*10_8))
            a_out(((2_8+J*10_8)+I*10_8*1_8))=a_out(((2_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((2_8+J*10_8))
            a_out(((3_8+J*10_8)+I*10_8*1_8))=a_out(((3_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((3_8+J*10_8))
            a_out(((4_8+J*10_8)+I*10_8*1_8))=a_out(((4_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((4_8+J*10_8))
            a_out(((5_8+J*10_8)+I*10_8*1_8))=a_out(((5_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((5_8+J*10_8))
            a_out(((6_8+J*10_8)+I*10_8*1_8))=a_out(((6_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((6_8+J*10_8))
            a_out(((7_8+J*10_8)+I*10_8*1_8))=a_out(((7_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((7_8+J*10_8))
            a_out(((8_8+J*10_8)+I*10_8*1_8))=a_out(((8_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((8_8+J*10_8))
            a_out(((9_8+J*10_8)+I*10_8*1_8))=a_out(((9_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((9_8+J*10_8))
            a_out(((10_8+J*10_8)+I*10_8*1_8))=a_out(((10_8+J*10_8)+I*10_8*1_8))+1.0_8*1.0_8*HALF_TRANS((10_8+J*10_8))
         enddo
         CART_TRANS(1_8)=a_in_3_0_0((((1_8+10_8*I)+1_8)-1_8))*1.0_8
         CART_TRANS(2_8)=a_in_3_0_0((((1_8+10_8*I)+2_8)-1_8))*1.0_8
         CART_TRANS(3_8)=a_in_3_0_0((((1_8+10_8*I)+3_8)-1_8))*1.0_8
         CART_TRANS(4_8)=a_in_3_0_0((((1_8+10_8*I)+4_8)-1_8))*1.0_8
         CART_TRANS(5_8)=a_in_3_0_0((((1_8+10_8*I)+5_8)-1_8))*1.0_8
         CART_TRANS(6_8)=a_in_3_0_0((((1_8+10_8*I)+6_8)-1_8))*1.0_8
         CART_TRANS(7_8)=a_in_3_0_0((((1_8+10_8*I)+7_8)-1_8))*1.0_8
         CART_TRANS(8_8)=a_in_3_0_0((((1_8+10_8*I)+8_8)-1_8))*1.0_8
         CART_TRANS(9_8)=a_in_3_0_0((((1_8+10_8*I)+9_8)-1_8))*1.0_8
         CART_TRANS(10_8)=a_in_3_0_0((((1_8+10_8*I)+10_8)-1_8))*1.0_8
         do J = 0_8, (1_8-1_8)
            HALF_TRANS((1_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*10_8))
            HALF_TRANS((2_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((2_8+J*10_8))
            HALF_TRANS((3_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((3_8+J*10_8))
            HALF_TRANS((4_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((4_8+J*10_8))
            HALF_TRANS((5_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((5_8+J*10_8))
            HALF_TRANS((6_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((6_8+J*10_8))
            HALF_TRANS((7_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((7_8+J*10_8))
            HALF_TRANS((8_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((8_8+J*10_8))
            HALF_TRANS((9_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((9_8+J*10_8))
            HALF_TRANS((10_8+J*10_8))=1.0_8*1.0_8*CART_TRANS((10_8+J*10_8))
         enddo
         do J = 1_8, 10_8
            a_out(((0_8+J)+I*1_8*10_8))=a_out(((0_8+J)+I*1_8*10_8))+1.0_8*1.0_8*centerB(2_8)*centerB(3_8)*HALF_TRANS((0_8+J))
         enddo
      enddo
!
   end subroutine HRR_wrapper_quadrupole_6_4_0_3_0_B_cc_final
!
end module HRR_wrapper_quadrupole_6_4_0_3_0

module vrr_x_7_4
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine vrr_xEF_one_7_4_r(AUX_INT)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    861
      use contract_module, only: contract_7_4_r
      use contract_module, only: contract_6_4_r
!!
      implicit none
!
      real(8), dimension(*), intent(inout) :: AUX_INT
      integer(8) :: k_1
      integer(8) :: k_2
!
      call contract_7_4_r(AUX_INT(7399_8:7406_8),AUX_INT(594_8:598_8),AUX_INT(9075_8:9086_8))
      call contract_7_4_r(AUX_INT(7407_8:7414_8),AUX_INT(599_8:603_8),AUX_INT(9087_8:9098_8))
      call contract_7_4_r(AUX_INT(7463_8:7470_8),AUX_INT(619_8:623_8),AUX_INT(9555_8:9566_8))
      call contract_6_4_r(AUX_INT(3597_8:3603_8),AUX_INT(594_8:598_8),AUX_INT(4925_8:4935_8))
      call contract_6_4_r(AUX_INT(3604_8:3610_8),AUX_INT(599_8:603_8),AUX_INT(4936_8:4946_8))
      call contract_6_4_r(AUX_INT(3646_8:3652_8),AUX_INT(619_8:623_8),AUX_INT(5310_8:5320_8))
      do k_1 = 0_8, 5_8
         AUX_INT(((2570_8+(5_8+k_1))-1_8))=AUX_INT(((1550_8+(1_8+k_1))-1_8))*AUX_INT(598_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((2570_8+(1_8+k_2))-1_8))=AUX_INT(1550_8)*AUX_INT(((594_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 3_8
            AUX_INT(((2570_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((2570_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1550_8+(1_8+k_1))-1_8))* &
            AUX_INT(((594_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((2580_8+(5_8+k_1))-1_8))=AUX_INT(((1556_8+(1_8+k_1))-1_8))*AUX_INT(603_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((2580_8+(1_8+k_2))-1_8))=AUX_INT(1556_8)*AUX_INT(((599_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 3_8
            AUX_INT(((2580_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((2580_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1556_8+(1_8+k_1))-1_8))* &
            AUX_INT(((599_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 5_8
         AUX_INT(((2870_8+(5_8+k_1))-1_8))=AUX_INT(((1586_8+(1_8+k_1))-1_8))*AUX_INT(623_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((2870_8+(1_8+k_2))-1_8))=AUX_INT(1586_8)*AUX_INT(((619_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 5_8
         do k_2 = 0_8, 3_8
            AUX_INT(((2870_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((2870_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((1586_8+(1_8+k_1))-1_8))* &
            AUX_INT(((619_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1316_8+(5_8+k_1))-1_8))=AUX_INT(((564_8+(1_8+k_1))-1_8))*AUX_INT(598_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((1316_8+(1_8+k_2))-1_8))=AUX_INT(564_8)*AUX_INT(((594_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 3_8
            AUX_INT(((1316_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1316_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((564_8+(1_8+k_1))-1_8))* &
            AUX_INT(((594_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1325_8+(5_8+k_1))-1_8))=AUX_INT(((569_8+(1_8+k_1))-1_8))*AUX_INT(603_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((1325_8+(1_8+k_2))-1_8))=AUX_INT(569_8)*AUX_INT(((599_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 3_8
            AUX_INT(((1325_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1325_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((569_8+(1_8+k_1))-1_8))* &
            AUX_INT(((599_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1541_8+(5_8+k_1))-1_8))=AUX_INT(((589_8+(1_8+k_1))-1_8))*AUX_INT(623_8)
      enddo
      do k_2 = 0_8, 3_8
         AUX_INT(((1541_8+(1_8+k_2))-1_8))=AUX_INT(589_8)*AUX_INT(((619_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 3_8
            AUX_INT(((1541_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1541_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((589_8+(1_8+k_1))-1_8))* &
            AUX_INT(((619_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1148_8+(4_8+k_1))-1_8))=AUX_INT(((594_8+(1_8+k_1))-1_8))*AUX_INT(162_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((1148_8+(1_8+k_2))-1_8))=AUX_INT(594_8)*AUX_INT(((159_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 2_8
            AUX_INT(((1148_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1148_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((594_8+(1_8+k_1))-1_8))* &
            AUX_INT(((159_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1156_8+(4_8+k_1))-1_8))=AUX_INT(((599_8+(1_8+k_1))-1_8))*AUX_INT(166_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((1156_8+(1_8+k_2))-1_8))=AUX_INT(599_8)*AUX_INT(((163_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 2_8
            AUX_INT(((1156_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1156_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((599_8+(1_8+k_1))-1_8))* &
            AUX_INT(((163_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((1308_8+(4_8+k_1))-1_8))=AUX_INT(((619_8+(1_8+k_1))-1_8))*AUX_INT(178_8)
      enddo
      do k_2 = 0_8, 2_8
         AUX_INT(((1308_8+(1_8+k_2))-1_8))=AUX_INT(619_8)*AUX_INT(((175_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 2_8
            AUX_INT(((1308_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((1308_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((619_8+(1_8+k_1))-1_8))* &
            AUX_INT(((175_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((868_8+(3_8+k_1))-1_8))=AUX_INT(((594_8+(1_8+k_1))-1_8))*AUX_INT(31_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((868_8+(1_8+k_2))-1_8))=AUX_INT(594_8)*AUX_INT(((29_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 1_8
            AUX_INT(((868_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((868_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((594_8+(1_8+k_1))-1_8))*AUX_INT &
            (((29_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((875_8+(3_8+k_1))-1_8))=AUX_INT(((599_8+(1_8+k_1))-1_8))*AUX_INT(34_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((875_8+(1_8+k_2))-1_8))=AUX_INT(599_8)*AUX_INT(((32_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 1_8
            AUX_INT(((875_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((875_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((599_8+(1_8+k_1))-1_8))*AUX_INT &
            (((32_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((973_8+(3_8+k_1))-1_8))=AUX_INT(((619_8+(1_8+k_1))-1_8))*AUX_INT(40_8)
      enddo
      do k_2 = 0_8, 1_8
         AUX_INT(((973_8+(1_8+k_2))-1_8))=AUX_INT(619_8)*AUX_INT(((38_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 1_8
            AUX_INT(((973_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((973_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((619_8+(1_8+k_1))-1_8))*AUX_INT &
            (((38_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((690_8+(2_8+k_1))-1_8))=AUX_INT(((594_8+(1_8+k_1))-1_8))*AUX_INT(3_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((690_8+(1_8+k_2))-1_8))=AUX_INT(594_8)*AUX_INT(((2_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 0_8
            AUX_INT(((690_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((690_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((594_8+(1_8+k_1))-1_8))*AUX_INT &
            (((2_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((696_8+(2_8+k_1))-1_8))=AUX_INT(((599_8+(1_8+k_1))-1_8))*AUX_INT(5_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((696_8+(1_8+k_2))-1_8))=AUX_INT(599_8)*AUX_INT(((4_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 0_8
            AUX_INT(((696_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((696_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((599_8+(1_8+k_1))-1_8))*AUX_INT &
            (((4_8+(1_8+k_2))-1_8))
         enddo
      enddo
      do k_1 = 0_8, 4_8
         AUX_INT(((750_8+(2_8+k_1))-1_8))=AUX_INT(((619_8+(1_8+k_1))-1_8))*AUX_INT(7_8)
      enddo
      do k_2 = 0_8, 0_8
         AUX_INT(((750_8+(1_8+k_2))-1_8))=AUX_INT(619_8)*AUX_INT(((6_8+(1_8+k_2))-1_8))
      enddo
      do k_1 = 1_8, 4_8
         do k_2 = 0_8, 0_8
            AUX_INT(((750_8+((1_8+k_1)+k_2))-1_8))=AUX_INT(((750_8+((1_8+k_1)+k_2))-1_8))+AUX_INT(((619_8+(1_8+k_1))-1_8))*AUX_INT &
            (((6_8+(1_8+k_2))-1_8))
         enddo
      enddo
!
   end subroutine vrr_xEF_one_7_4_r
   subroutine vrr_xEF_two_7_4_r(AUX_INT,I_TWOG1G2_POWER)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    462
!!
      implicit none
!
      real(8), dimension(*), intent(inout) :: AUX_INT
      real(8), dimension(4), intent(in) :: I_TWOG1G2_POWER
!
      AUX_INT(9076_8:9085_8)=AUX_INT(9076_8:9085_8)+28.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(4345_8:4354_8)
      AUX_INT(9077_8:9084_8)=AUX_INT(9077_8:9084_8)+252.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(1816_8:1823_8)
      AUX_INT(9078_8:9083_8)=AUX_INT(9078_8:9083_8)+840.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(624_8:629_8)
      AUX_INT(9079_8:9082_8)=AUX_INT(9079_8:9082_8)+840.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(159_8:162_8)
      AUX_INT(9088_8:9097_8)=AUX_INT(9088_8:9097_8)+28.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(4355_8:4364_8)
      AUX_INT(9089_8:9096_8)=AUX_INT(9089_8:9096_8)+252.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(1824_8:1831_8)
      AUX_INT(9090_8:9095_8)=AUX_INT(9090_8:9095_8)+840.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(630_8:635_8)
      AUX_INT(9091_8:9094_8)=AUX_INT(9091_8:9094_8)+840.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(163_8:166_8)
      AUX_INT(9556_8:9565_8)=AUX_INT(9556_8:9565_8)+28.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(4625_8:4634_8)
      AUX_INT(9557_8:9564_8)=AUX_INT(9557_8:9564_8)+252.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(1960_8:1967_8)
      AUX_INT(9558_8:9563_8)=AUX_INT(9558_8:9563_8)+840.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(684_8:689_8)
      AUX_INT(9559_8:9562_8)=AUX_INT(9559_8:9562_8)+840.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(175_8:178_8)
      AUX_INT(4926_8:4934_8)=AUX_INT(4926_8:4934_8)+24.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(2120_8:2128_8)
      AUX_INT(4927_8:4933_8)=AUX_INT(4927_8:4933_8)+180.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(756_8:762_8)
      AUX_INT(4928_8:4932_8)=AUX_INT(4928_8:4932_8)+480.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(199_8:203_8)
      AUX_INT(4929_8:4931_8)=AUX_INT(4929_8:4931_8)+360.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(29_8:31_8)
      AUX_INT(4937_8:4945_8)=AUX_INT(4937_8:4945_8)+24.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(2129_8:2137_8)
      AUX_INT(4938_8:4944_8)=AUX_INT(4938_8:4944_8)+180.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(763_8:769_8)
      AUX_INT(4939_8:4943_8)=AUX_INT(4939_8:4943_8)+480.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(204_8:208_8)
      AUX_INT(4940_8:4942_8)=AUX_INT(4940_8:4942_8)+360.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(32_8:34_8)
      AUX_INT(5311_8:5319_8)=AUX_INT(5311_8:5319_8)+24.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(2336_8:2344_8)
      AUX_INT(5312_8:5318_8)=AUX_INT(5312_8:5318_8)+180.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(861_8:867_8)
      AUX_INT(5313_8:5317_8)=AUX_INT(5313_8:5317_8)+480.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(239_8:243_8)
      AUX_INT(5314_8:5316_8)=AUX_INT(5314_8:5316_8)+360.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(38_8:40_8)
      AUX_INT(2571_8:2578_8)=AUX_INT(2571_8:2578_8)+20.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(980_8:987_8)
      AUX_INT(2572_8:2577_8)=AUX_INT(2572_8:2577_8)+120.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(289_8:294_8)
      AUX_INT(2573_8:2576_8)=AUX_INT(2573_8:2576_8)+240.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(53_8:56_8)
      AUX_INT(2574_8:2575_8)=AUX_INT(2574_8:2575_8)+120.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(2_8:3_8)
      AUX_INT(2581_8:2588_8)=AUX_INT(2581_8:2588_8)+20.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(988_8:995_8)
      AUX_INT(2582_8:2587_8)=AUX_INT(2582_8:2587_8)+120.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(295_8:300_8)
      AUX_INT(2583_8:2586_8)=AUX_INT(2583_8:2586_8)+240.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(57_8:60_8)
      AUX_INT(2584_8:2585_8)=AUX_INT(2584_8:2585_8)+120.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(4_8:5_8)
      AUX_INT(2871_8:2878_8)=AUX_INT(2871_8:2878_8)+20.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(1140_8:1147_8)
      AUX_INT(2872_8:2877_8)=AUX_INT(2872_8:2877_8)+120.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(361_8:366_8)
      AUX_INT(2873_8:2876_8)=AUX_INT(2873_8:2876_8)+240.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(77_8:80_8)
      AUX_INT(2874_8:2875_8)=AUX_INT(2874_8:2875_8)+120.0_8*I_TWOG1G2_POWER(4_8)*AUX_INT(6_8:7_8)
      AUX_INT(1317_8:1323_8)=AUX_INT(1317_8:1323_8)+16.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(445_8:451_8)
      AUX_INT(1318_8:1322_8)=AUX_INT(1318_8:1322_8)+72.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(109_8:113_8)
      AUX_INT(1319_8:1321_8)=AUX_INT(1319_8:1321_8)+96.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(14_8:16_8)
      AUX_INT(1320_8)=AUX_INT(1320_8)+24.0_8*I_TWOG1G2_POWER(4_8)
      AUX_INT(1326_8:1332_8)=AUX_INT(1326_8:1332_8)+16.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(452_8:458_8)
      AUX_INT(1327_8:1331_8)=AUX_INT(1327_8:1331_8)+72.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(114_8:118_8)
      AUX_INT(1328_8:1330_8)=AUX_INT(1328_8:1330_8)+96.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(17_8:19_8)
      AUX_INT(1329_8)=AUX_INT(1329_8)+24.0_8*I_TWOG1G2_POWER(4_8)
      AUX_INT(1542_8:1548_8)=AUX_INT(1542_8:1548_8)+16.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(557_8:563_8)
      AUX_INT(1543_8:1547_8)=AUX_INT(1543_8:1547_8)+72.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(154_8:158_8)
      AUX_INT(1544_8:1546_8)=AUX_INT(1544_8:1546_8)+96.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(26_8:28_8)
      AUX_INT(1545_8)=AUX_INT(1545_8)+24.0_8*I_TWOG1G2_POWER(4_8)
      AUX_INT(1149_8:1154_8)=AUX_INT(1149_8:1154_8)+12.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(367_8:372_8)
      AUX_INT(1150_8:1153_8)=AUX_INT(1150_8:1153_8)+36.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(81_8:84_8)
      AUX_INT(1151_8:1152_8)=AUX_INT(1151_8:1152_8)+24.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(8_8:9_8)
      AUX_INT(1157_8:1162_8)=AUX_INT(1157_8:1162_8)+12.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(373_8:378_8)
      AUX_INT(1158_8:1161_8)=AUX_INT(1158_8:1161_8)+36.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(85_8:88_8)
      AUX_INT(1159_8:1160_8)=AUX_INT(1159_8:1160_8)+24.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(10_8:11_8)
      AUX_INT(1309_8:1314_8)=AUX_INT(1309_8:1314_8)+12.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(439_8:444_8)
      AUX_INT(1310_8:1313_8)=AUX_INT(1310_8:1313_8)+36.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(105_8:108_8)
      AUX_INT(1311_8:1312_8)=AUX_INT(1311_8:1312_8)+24.0_8*I_TWOG1G2_POWER(3_8)*AUX_INT(12_8:13_8)
      AUX_INT(869_8:873_8)=AUX_INT(869_8:873_8)+8.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(244_8:248_8)
      AUX_INT(870_8:872_8)=AUX_INT(870_8:872_8)+12.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(41_8:43_8)
      AUX_INT(876_8:880_8)=AUX_INT(876_8:880_8)+8.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(249_8:253_8)
      AUX_INT(877_8:879_8)=AUX_INT(877_8:879_8)+12.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(44_8:46_8)
      AUX_INT(974_8:978_8)=AUX_INT(974_8:978_8)+8.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(284_8:288_8)
      AUX_INT(975_8:977_8)=AUX_INT(975_8:977_8)+12.0_8*I_TWOG1G2_POWER(2_8)*AUX_INT(50_8:52_8)
      AUX_INT(691_8:694_8)=AUX_INT(691_8:694_8)+4.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(179_8:182_8)
      AUX_INT(697_8:700_8)=AUX_INT(697_8:700_8)+4.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(183_8:186_8)
      AUX_INT(751_8:754_8)=AUX_INT(751_8:754_8)+4.0_8*I_TWOG1G2_POWER(1_8)*AUX_INT(195_8:198_8)
!
   end subroutine vrr_xEF_two_7_4_r
!
end module vrr_x_7_4

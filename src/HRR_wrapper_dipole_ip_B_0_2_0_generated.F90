module HRR_wrapper_dipole_ip_B_0_2_0
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine HRR_wrapper_dipole_ip_B_0_2_0_B_pp_final(dim,a_in_2_0_1,a_in_3_0_1,a_in_4_0_1,a_out,centerA,centerB)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    398
      use hrr_cart_2_2_B, only: hrr_cart_2_2_B_r
      use hrr_cart_2_1_B, only: hrr_cart_2_1_B_r
!!
      implicit none
!
      integer(8), intent(in) :: dim
      real(8), dimension(6_8*dim), intent(in) :: a_in_2_0_1
      real(8), dimension(10_8*dim), intent(in) :: a_in_3_0_1
      real(8), dimension(15_8*dim), intent(in) :: a_in_4_0_1
      real(8), dimension(*), intent(out) :: a_out
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      integer(8) :: I
      real(8), dimension(9) :: AB
      real(8), dimension(30) :: HALF_TRANS
      real(8), dimension(36) :: CART_TRANS
      integer(8) :: J
!
      AB(1_8:3_8)=centerA-centerB
      AB(4_8)=AB(3_8)*AB(1_8)
      AB(5_8)=AB(2_8)*AB(1_8)
      AB(6_8)=AB(1_8)*AB(1_8)
      AB(7_8)=AB(3_8)*AB(2_8)
      AB(8_8)=AB(2_8)*AB(2_8)
      AB(9_8)=AB(3_8)*AB(3_8)
      do I = 0_8, (dim-1_8)
         call hrr_cart_2_2_B_r(AB,CART_TRANS,a_in_2_0_1((1_8+6_8*I):6_8*(I+1_8)),a_in_3_0_1((1_8+10_8*I):10_8*(I+1_8)),a_in_4_0_1( &
         (1_8+15_8*I):15_8*(I+1_8)))
         do J = 0_8, (6_8-1_8)
            HALF_TRANS((1_8+J*5_8))=1.0_8*1.732050807568878_8*CART_TRANS((2_8+J*6_8))
            HALF_TRANS((2_8+J*5_8))=1.0_8*1.732050807568878_8*CART_TRANS((5_8+J*6_8))
            HALF_TRANS((3_8+J*5_8))=(1.0_8*(-0.5_8)*CART_TRANS((1_8+J*6_8))+1.0_8*(-0.5_8)*CART_TRANS((4_8+J*6_8)))+1.0_8*1.0_8* &
            CART_TRANS((6_8+J*6_8))
            HALF_TRANS((4_8+J*5_8))=1.0_8*1.732050807568878_8*CART_TRANS((3_8+J*6_8))
            HALF_TRANS((5_8+J*5_8))=1.0_8*0.866025403784439_8*CART_TRANS((1_8+J*6_8))+1.0_8*(-0.866025403784439_8)*CART_TRANS((4_8 &
            +J*6_8))
         enddo
         do J = 1_8, 5_8
            a_out(((0_8+J)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((0_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+5_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((1_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+10_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((2_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+15_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((1_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+20_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((3_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+25_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((4_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+30_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((2_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+35_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((4_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+40_8*dim)+I*1_8*5_8))=1.0_8*(-2.0_8)*HALF_TRANS((5_8*5_8+J))
         enddo
         call hrr_cart_2_1_B_r(AB,CART_TRANS,a_in_2_0_1((1_8+6_8*I):6_8*(I+1_8)),a_in_3_0_1((1_8+10_8*I):10_8*(I+1_8)))
         do J = 0_8, (3_8-1_8)
            HALF_TRANS((1_8+J*5_8))=1.0_8*1.732050807568878_8*CART_TRANS((2_8+J*6_8))
            HALF_TRANS((2_8+J*5_8))=1.0_8*1.732050807568878_8*CART_TRANS((5_8+J*6_8))
            HALF_TRANS((3_8+J*5_8))=(1.0_8*(-0.5_8)*CART_TRANS((1_8+J*6_8))+1.0_8*(-0.5_8)*CART_TRANS((4_8+J*6_8)))+1.0_8*1.0_8* &
            CART_TRANS((6_8+J*6_8))
            HALF_TRANS((4_8+J*5_8))=1.0_8*1.732050807568878_8*CART_TRANS((3_8+J*6_8))
            HALF_TRANS((5_8+J*5_8))=1.0_8*0.866025403784439_8*CART_TRANS((1_8+J*6_8))+1.0_8*(-0.866025403784439_8)*CART_TRANS((4_8 &
            +J*6_8))
         enddo
         do J = 1_8, 5_8
            a_out(((0_8+J)+I*1_8*5_8))=a_out(((0_8+J)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(1_8)*HALF_TRANS((0_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+5_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+5_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(1_8)*HALF_TRANS &
            ((1_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+10_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+10_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(1_8)* &
            HALF_TRANS((2_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+15_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+15_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(2_8)* &
            HALF_TRANS((0_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+20_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+20_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(2_8)* &
            HALF_TRANS((1_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+25_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+25_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(2_8)* &
            HALF_TRANS((2_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+30_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+30_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(3_8)* &
            HALF_TRANS((0_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+35_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+35_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(3_8)* &
            HALF_TRANS((1_8*5_8+J))
         enddo
         do J = 1_8, 5_8
            a_out((((0_8+J)+40_8*dim)+I*1_8*5_8))=a_out((((0_8+J)+40_8*dim)+I*1_8*5_8))+1.0_8*1.0_8*(-2.0_8)*centerB(3_8)* &
            HALF_TRANS((2_8*5_8+J))
         enddo
      enddo
!
   end subroutine HRR_wrapper_dipole_ip_B_0_2_0_B_pp_final
   subroutine HRR_wrapper_dipole_ip_B_0_2_0_B_cc_final(dim,a_in_2_0_1,a_in_3_0_1,a_in_4_0_1,a_out,centerA,centerB)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    384
      use hrr_cart_2_2_B, only: hrr_cart_2_2_B_r
      use hrr_cart_2_1_B, only: hrr_cart_2_1_B_r
!!
      implicit none
!
      integer(8), intent(in) :: dim
      real(8), dimension(6_8*dim), intent(in) :: a_in_2_0_1
      real(8), dimension(10_8*dim), intent(in) :: a_in_3_0_1
      real(8), dimension(15_8*dim), intent(in) :: a_in_4_0_1
      real(8), dimension(*), intent(out) :: a_out
      real(8), dimension(3), intent(in) :: centerA
      real(8), dimension(3), intent(in) :: centerB
      integer(8) :: I
      real(8), dimension(9) :: AB
      real(8), dimension(36) :: HALF_TRANS
      real(8), dimension(36) :: CART_TRANS
      integer(8) :: J
!
      AB(1_8:3_8)=centerA-centerB
      AB(4_8)=AB(3_8)*AB(1_8)
      AB(5_8)=AB(2_8)*AB(1_8)
      AB(6_8)=AB(1_8)*AB(1_8)
      AB(7_8)=AB(3_8)*AB(2_8)
      AB(8_8)=AB(2_8)*AB(2_8)
      AB(9_8)=AB(3_8)*AB(3_8)
      do I = 0_8, (dim-1_8)
         call hrr_cart_2_2_B_r(AB,CART_TRANS,a_in_2_0_1((1_8+6_8*I):6_8*(I+1_8)),a_in_3_0_1((1_8+10_8*I):10_8*(I+1_8)),a_in_4_0_1( &
         (1_8+15_8*I):15_8*(I+1_8)))
         do J = 0_8, (6_8-1_8)
            HALF_TRANS((1_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*6_8))
            HALF_TRANS((2_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((2_8+J*6_8))
            HALF_TRANS((3_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((3_8+J*6_8))
            HALF_TRANS((4_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((4_8+J*6_8))
            HALF_TRANS((5_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((5_8+J*6_8))
            HALF_TRANS((6_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((6_8+J*6_8))
         enddo
         do J = 1_8, 6_8
            a_out(((0_8+J)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((0_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+6_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((1_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+12_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((2_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+18_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((1_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+24_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((3_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+30_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((4_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+36_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((2_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+42_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((4_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+48_8*dim)+I*1_8*6_8))=1.0_8*(-2.0_8)*HALF_TRANS((5_8*6_8+J))
         enddo
         call hrr_cart_2_1_B_r(AB,CART_TRANS,a_in_2_0_1((1_8+6_8*I):6_8*(I+1_8)),a_in_3_0_1((1_8+10_8*I):10_8*(I+1_8)))
         do J = 0_8, (3_8-1_8)
            HALF_TRANS((1_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((1_8+J*6_8))
            HALF_TRANS((2_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((2_8+J*6_8))
            HALF_TRANS((3_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((3_8+J*6_8))
            HALF_TRANS((4_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((4_8+J*6_8))
            HALF_TRANS((5_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((5_8+J*6_8))
            HALF_TRANS((6_8+J*6_8))=1.0_8*1.0_8*CART_TRANS((6_8+J*6_8))
         enddo
         do J = 1_8, 6_8
            a_out(((0_8+J)+I*1_8*6_8))=a_out(((0_8+J)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(1_8)*HALF_TRANS((0_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+6_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+6_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(1_8)*HALF_TRANS((1_8* &
            6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+12_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+12_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(1_8)*HALF_TRANS(( &
            2_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+18_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+18_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(2_8)*HALF_TRANS(( &
            0_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+24_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+24_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(2_8)*HALF_TRANS(( &
            1_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+30_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+30_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(2_8)*HALF_TRANS(( &
            2_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+36_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+36_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(3_8)*HALF_TRANS(( &
            0_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+42_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+42_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(3_8)*HALF_TRANS(( &
            1_8*6_8+J))
         enddo
         do J = 1_8, 6_8
            a_out((((0_8+J)+48_8*dim)+I*1_8*6_8))=a_out((((0_8+J)+48_8*dim)+I*1_8*6_8))+1.0_8*(-2.0_8)*centerB(3_8)*HALF_TRANS(( &
            2_8*6_8+J))
         enddo
      enddo
!
   end subroutine HRR_wrapper_dipole_ip_B_0_2_0_B_cc_final
!
end module HRR_wrapper_dipole_ip_B_0_2_0

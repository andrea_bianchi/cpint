module primitive_loop_dia_0_module
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine primitive_loop_dia_0(g_shell,lA,n_primA,coeff_and_expA,centerA,is_pureA,lB,n_primB,coeff_and_expB,centerB,is_pureB, &
   threshold,prefactor,skipped,n_external_points,external_points_charge,external_points_coord)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
      use vrr_pre_intermediates_3_0, only: vrr_pre_intermediates_3_0_r
      use vrr_eri_explict_x_yz_8, only: vrr_eri_get_range_1174_b_r
      use parameters_module, only: PURE_LENGTH
      use parameters_module, only: CART_LENGTH
      use HRR_wrapper_dia_0_0_0, only: HRR_wrapper_dia_0_0_0_A_pp_final
      use HRR_wrapper_dia_1_0_0, only: HRR_wrapper_dia_1_0_0_A_pp_final
      use HRR_wrapper_dia_0_0_0, only: HRR_wrapper_dia_0_0_0_A_cc_final
      use HRR_wrapper_dia_1_0_0, only: HRR_wrapper_dia_1_0_0_A_cc_final
      use boys_real_3, only: get_boys_3_real
!!
      implicit none
!
      real(8), dimension( *), intent(inout) :: g_shell
      integer(8), intent(in) :: lA
      integer(8), intent(in) :: n_primA
      real(8), dimension(2, n_primA), intent(in) :: coeff_and_expA
      real(8), dimension(3), intent(in) :: centerA
      logical(8), intent(in) :: is_pureA
      integer(8), intent(in) :: lB
      integer(8), intent(in) :: n_primB
      real(8), dimension(2, n_primB), intent(in) :: coeff_and_expB
      real(8), dimension(3), intent(in) :: centerB
      logical(8), intent(in) :: is_pureB
      real(8), intent(in) :: threshold
      real(8), intent(in) :: prefactor
      logical(8), intent(out) :: skipped
      integer(8), intent(in) :: n_external_points
      real(8), dimension(n_external_points), intent(in) :: external_points_charge
      real(8), dimension(3, n_external_points), intent(in) :: external_points_coord
      real(8), dimension(4) :: boys_function
      real(8), dimension(16) :: contracted_integral
      integer(8) :: max_l_bra
      real(8), dimension(3) :: highest_angmom_coord
      real(8), dimension(27) :: in_vrr
      real(8) :: AB2
      integer(8) :: i_prim_A
      integer(8) :: i_prim_B
      real(8) :: gamma
      real(8) :: inv_gamma
      real(8), dimension(3) :: P_coord
      real(8) :: reference_value
      real(8) :: final_prefactor
      integer(8) :: external_point
      real(8), dimension(178) :: auxiliary_integral_yz
      real(8), dimension(147) :: contracted_x_boys
      real(8), dimension(10) :: uncontracted_integral
      integer(8) :: dim_integral_AB
      real(8), dimension(9) :: tmp_g_shell
!
      boys_function=0.0_8
      contracted_integral=0.0_8
      if (((lA+1_8).ge.(lB+2_8))) then
         max_l_bra=lA+1_8
         highest_angmom_coord(1_8:3_8)=centerA(:)
      else 
         max_l_bra=lB+2_8
         highest_angmom_coord(1_8:3_8)=centerB(:)
      endif 
      in_vrr=0.0_8
      in_vrr(1_8:3_8)=highest_angmom_coord(1_8:3_8)
      in_vrr(9_8)=1.0_8
      in_vrr(11_8)=1.0_8
      AB2=(centerA(1_8)-centerB(1_8))**2_8
      AB2=AB2+(centerA(2_8)-centerB(2_8))**2_8
      AB2=AB2+(centerA(3_8)-centerB(3_8))**2_8
      do i_prim_A = 1_8, n_primA
         do i_prim_B = 1_8, n_primB
            gamma=coeff_and_expA(2_8,i_prim_A)+coeff_and_expB(2_8,i_prim_B)
            inv_gamma=1.0_8/gamma
            P_coord=(coeff_and_expA(2_8,i_prim_A)*centerA(:)+coeff_and_expB(2_8,i_prim_B)*centerB(:))*inv_gamma
            reference_value=threshold
            final_prefactor=3.141592653589793_8*prefactor*coeff_and_expA(1_8,i_prim_A)*coeff_and_expB(1_8,i_prim_B)*inv_gamma*exp( &
            -coeff_and_expA(2_8,i_prim_A)*coeff_and_expB(2_8,i_prim_B)*inv_gamma*AB2)
            if ((abs(final_prefactor).le.threshold*reference_value)) then
               cycle
            endif 
            reference_value=max(abs(final_prefactor),reference_value)
            do external_point = 1_8, n_external_points
               if ((abs(external_points_charge(external_point)).lt.threshold)) then
                  cycle
               endif 
               call get_boys_3_real(boys_function,gamma*(((P_coord(1_8)-external_points_coord(1_8,external_point))**2_8+(P_coord( &
               2_8)-external_points_coord(2_8,external_point))**2_8)+(P_coord(3_8)-external_points_coord(3_8,external_point))**2_8 &
               ))
               boys_function=final_prefactor*external_points_charge(external_point)*boys_function
               in_vrr(12_8:14_8)=P_coord(1_8:3_8)
               in_vrr(8_8)=inv_gamma
               in_vrr(18_8:20_8)=P_coord(1_8:3_8)-external_points_coord(1_8:3_8,external_point)
               call vrr_pre_intermediates_3_0_r(boys_function,in_vrr,auxiliary_integral_yz,contracted_x_boys)
               select case (lA)
                  case (0_8)
                     call vrr_eri_get_range_1174_b_r(boys_function,contracted_x_boys,auxiliary_integral_yz,uncontracted_integral)
                     contracted_integral(1_8:3_8)=contracted_integral(1_8:3_8)+1.0_8*coeff_and_expA(2_8,i_prim_A)* &
                     uncontracted_integral(1_8:3_8)
                     contracted_integral(4_8:9_8)=contracted_integral(4_8:9_8)+1.0_8*coeff_and_expA(2_8,i_prim_A)* &
                     uncontracted_integral(4_8:9_8)
                     contracted_integral(11_8:16_8)=contracted_integral(11_8:16_8)+1.0_8*coeff_and_expB(2_8,i_prim_B)* &
                     uncontracted_integral(4_8:9_8)
                     contracted_integral(10_8:10_8)=contracted_integral(10_8:10_8)+1.0_8*uncontracted_integral(10_8:10_8)
               end select
            enddo
         enddo
      enddo
      if ((boys_function(1_8).eq.0.0_8)) then
         skipped=.True._8
         if (is_pureA) then
            dim_integral_AB=PURE_LENGTH(lA)
         else 
            dim_integral_AB=CART_LENGTH(lA)
         endif 
         if (is_pureB) then
            dim_integral_AB=dim_integral_AB*PURE_LENGTH(lB)
         else 
            dim_integral_AB=dim_integral_AB*CART_LENGTH(lB)
         endif 
         g_shell(1_8:dim_integral_AB*18_8)=0.0_8
      else 
         skipped=.False._8
         if (is_pureA) then
            select case (lA)
               case (0_8)
                  call HRR_wrapper_dia_0_0_0_A_pp_final(1_8,contracted_integral(1_8:3_8),contracted_integral(4_8:9_8),g_shell(1_8: &
                  9_8),centerA,centerB)
                  call HRR_wrapper_dia_1_0_0_A_pp_final(1_8,contracted_integral(10_8:10_8),contracted_integral(11_8:16_8), &
                  tmp_g_shell(1_8:9_8),centerA,centerB)
                  g_shell(1_8:9_8)=g_shell(1_8:9_8)+tmp_g_shell(1_8:9_8)
            end select
         else 
            select case (lA)
               case (0_8)
                  call HRR_wrapper_dia_0_0_0_A_cc_final(1_8,contracted_integral(1_8:3_8),contracted_integral(4_8:9_8),g_shell(1_8: &
                  9_8),centerA,centerB)
                  call HRR_wrapper_dia_1_0_0_A_cc_final(1_8,contracted_integral(10_8:10_8),contracted_integral(11_8:16_8), &
                  tmp_g_shell(1_8:9_8),centerA,centerB)
                  g_shell(1_8:9_8)=g_shell(1_8:9_8)+tmp_g_shell(1_8:9_8)
            end select
         endif 
      endif 
!
   end subroutine primitive_loop_dia_0
!
end module primitive_loop_dia_0_module

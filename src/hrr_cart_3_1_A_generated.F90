module hrr_cart_3_1_A
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine hrr_cart_3_1_A_r(AB,OUT_INT,a_in_1,a_in_2,a_in_3,a_in_4)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    336
!!
      implicit none
!
      real(8), dimension(19), intent(in) :: AB
      real(8), dimension(30), intent(out) :: OUT_INT
      real(8), dimension(3), intent(in) :: a_in_1
      real(8), dimension(6), intent(in) :: a_in_2
      real(8), dimension(10), intent(in) :: a_in_3
      real(8), dimension(15), intent(in) :: a_in_4
!
      OUT_INT(1_8)=((a_in_1(1_8)*1.0_8*AB(15_8)+a_in_2(1_8)*3.0_8*AB(6_8))+a_in_3(1_8)*3.0_8*AB(1_8))+a_in_4(1_8)*1.0_8
      OUT_INT(2_8)=((((a_in_1(1_8)*1.0_8*AB(14_8)+a_in_2(1_8)*2.0_8*AB(5_8))+a_in_2(2_8)*1.0_8*AB(6_8))+a_in_3(1_8)*1.0_8*AB(2_8)) &
      +a_in_3(2_8)*2.0_8*AB(1_8))+a_in_4(2_8)*1.0_8
      OUT_INT(3_8)=((((a_in_1(1_8)*1.0_8*AB(13_8)+a_in_2(1_8)*2.0_8*AB(4_8))+a_in_2(3_8)*1.0_8*AB(6_8))+a_in_3(1_8)*1.0_8*AB(3_8)) &
      +a_in_3(3_8)*2.0_8*AB(1_8))+a_in_4(3_8)*1.0_8
      OUT_INT(4_8)=((((a_in_1(1_8)*1.0_8*AB(12_8)+a_in_2(1_8)*1.0_8*AB(8_8))+a_in_2(2_8)*2.0_8*AB(5_8))+a_in_3(2_8)*2.0_8*AB(2_8)) &
      +a_in_3(4_8)*1.0_8*AB(1_8))+a_in_4(4_8)*1.0_8
      OUT_INT(5_8)=((((((a_in_1(1_8)*1.0_8*AB(11_8)+a_in_2(1_8)*1.0_8*AB(7_8))+a_in_2(2_8)*1.0_8*AB(4_8))+a_in_2(3_8)*1.0_8*AB(5_8 &
      ))+a_in_3(2_8)*1.0_8*AB(3_8))+a_in_3(3_8)*1.0_8*AB(2_8))+a_in_3(5_8)*1.0_8*AB(1_8))+a_in_4(5_8)*1.0_8
      OUT_INT(6_8)=((((a_in_1(1_8)*1.0_8*AB(10_8)+a_in_2(1_8)*1.0_8*AB(9_8))+a_in_2(3_8)*2.0_8*AB(4_8))+a_in_3(3_8)*2.0_8*AB(3_8)) &
      +a_in_3(6_8)*1.0_8*AB(1_8))+a_in_4(6_8)*1.0_8
      OUT_INT(7_8)=((a_in_1(1_8)*1.0_8*AB(18_8)+a_in_2(2_8)*3.0_8*AB(8_8))+a_in_3(4_8)*3.0_8*AB(2_8))+a_in_4(7_8)*1.0_8
      OUT_INT(8_8)=((((a_in_1(1_8)*1.0_8*AB(17_8)+a_in_2(2_8)*2.0_8*AB(7_8))+a_in_2(3_8)*1.0_8*AB(8_8))+a_in_3(4_8)*1.0_8*AB(3_8)) &
      +a_in_3(5_8)*2.0_8*AB(2_8))+a_in_4(8_8)*1.0_8
      OUT_INT(9_8)=((((a_in_1(1_8)*1.0_8*AB(16_8)+a_in_2(2_8)*1.0_8*AB(9_8))+a_in_2(3_8)*2.0_8*AB(7_8))+a_in_3(5_8)*2.0_8*AB(3_8)) &
      +a_in_3(6_8)*1.0_8*AB(2_8))+a_in_4(9_8)*1.0_8
      OUT_INT(10_8)=((a_in_1(1_8)*1.0_8*AB(19_8)+a_in_2(3_8)*3.0_8*AB(9_8))+a_in_3(6_8)*3.0_8*AB(3_8))+a_in_4(10_8)*1.0_8
      OUT_INT(11_8)=((a_in_1(2_8)*1.0_8*AB(15_8)+a_in_2(2_8)*3.0_8*AB(6_8))+a_in_3(2_8)*3.0_8*AB(1_8))+a_in_4(2_8)*1.0_8
      OUT_INT(12_8)=((((a_in_1(2_8)*1.0_8*AB(14_8)+a_in_2(2_8)*2.0_8*AB(5_8))+a_in_2(4_8)*1.0_8*AB(6_8))+a_in_3(2_8)*1.0_8*AB(2_8) &
      )+a_in_3(4_8)*2.0_8*AB(1_8))+a_in_4(4_8)*1.0_8
      OUT_INT(13_8)=((((a_in_1(2_8)*1.0_8*AB(13_8)+a_in_2(2_8)*2.0_8*AB(4_8))+a_in_2(5_8)*1.0_8*AB(6_8))+a_in_3(2_8)*1.0_8*AB(3_8) &
      )+a_in_3(5_8)*2.0_8*AB(1_8))+a_in_4(5_8)*1.0_8
      OUT_INT(14_8)=((((a_in_1(2_8)*1.0_8*AB(12_8)+a_in_2(2_8)*1.0_8*AB(8_8))+a_in_2(4_8)*2.0_8*AB(5_8))+a_in_3(4_8)*2.0_8*AB(2_8) &
      )+a_in_3(7_8)*1.0_8*AB(1_8))+a_in_4(7_8)*1.0_8
      OUT_INT(15_8)=((((((a_in_1(2_8)*1.0_8*AB(11_8)+a_in_2(2_8)*1.0_8*AB(7_8))+a_in_2(4_8)*1.0_8*AB(4_8))+a_in_2(5_8)*1.0_8*AB( &
      5_8))+a_in_3(4_8)*1.0_8*AB(3_8))+a_in_3(5_8)*1.0_8*AB(2_8))+a_in_3(8_8)*1.0_8*AB(1_8))+a_in_4(8_8)*1.0_8
      OUT_INT(16_8)=((((a_in_1(2_8)*1.0_8*AB(10_8)+a_in_2(2_8)*1.0_8*AB(9_8))+a_in_2(5_8)*2.0_8*AB(4_8))+a_in_3(5_8)*2.0_8*AB(3_8) &
      )+a_in_3(9_8)*1.0_8*AB(1_8))+a_in_4(9_8)*1.0_8
      OUT_INT(17_8)=((a_in_1(2_8)*1.0_8*AB(18_8)+a_in_2(4_8)*3.0_8*AB(8_8))+a_in_3(7_8)*3.0_8*AB(2_8))+a_in_4(11_8)*1.0_8
      OUT_INT(18_8)=((((a_in_1(2_8)*1.0_8*AB(17_8)+a_in_2(4_8)*2.0_8*AB(7_8))+a_in_2(5_8)*1.0_8*AB(8_8))+a_in_3(7_8)*1.0_8*AB(3_8) &
      )+a_in_3(8_8)*2.0_8*AB(2_8))+a_in_4(12_8)*1.0_8
      OUT_INT(19_8)=((((a_in_1(2_8)*1.0_8*AB(16_8)+a_in_2(4_8)*1.0_8*AB(9_8))+a_in_2(5_8)*2.0_8*AB(7_8))+a_in_3(8_8)*2.0_8*AB(3_8) &
      )+a_in_3(9_8)*1.0_8*AB(2_8))+a_in_4(13_8)*1.0_8
      OUT_INT(20_8)=((a_in_1(2_8)*1.0_8*AB(19_8)+a_in_2(5_8)*3.0_8*AB(9_8))+a_in_3(9_8)*3.0_8*AB(3_8))+a_in_4(14_8)*1.0_8
      OUT_INT(21_8)=((a_in_1(3_8)*1.0_8*AB(15_8)+a_in_2(3_8)*3.0_8*AB(6_8))+a_in_3(3_8)*3.0_8*AB(1_8))+a_in_4(3_8)*1.0_8
      OUT_INT(22_8)=((((a_in_1(3_8)*1.0_8*AB(14_8)+a_in_2(3_8)*2.0_8*AB(5_8))+a_in_2(5_8)*1.0_8*AB(6_8))+a_in_3(3_8)*1.0_8*AB(2_8) &
      )+a_in_3(5_8)*2.0_8*AB(1_8))+a_in_4(5_8)*1.0_8
      OUT_INT(23_8)=((((a_in_1(3_8)*1.0_8*AB(13_8)+a_in_2(3_8)*2.0_8*AB(4_8))+a_in_2(6_8)*1.0_8*AB(6_8))+a_in_3(3_8)*1.0_8*AB(3_8) &
      )+a_in_3(6_8)*2.0_8*AB(1_8))+a_in_4(6_8)*1.0_8
      OUT_INT(24_8)=((((a_in_1(3_8)*1.0_8*AB(12_8)+a_in_2(3_8)*1.0_8*AB(8_8))+a_in_2(5_8)*2.0_8*AB(5_8))+a_in_3(5_8)*2.0_8*AB(2_8) &
      )+a_in_3(8_8)*1.0_8*AB(1_8))+a_in_4(8_8)*1.0_8
      OUT_INT(25_8)=((((((a_in_1(3_8)*1.0_8*AB(11_8)+a_in_2(3_8)*1.0_8*AB(7_8))+a_in_2(5_8)*1.0_8*AB(4_8))+a_in_2(6_8)*1.0_8*AB( &
      5_8))+a_in_3(5_8)*1.0_8*AB(3_8))+a_in_3(6_8)*1.0_8*AB(2_8))+a_in_3(9_8)*1.0_8*AB(1_8))+a_in_4(9_8)*1.0_8
      OUT_INT(26_8)=((((a_in_1(3_8)*1.0_8*AB(10_8)+a_in_2(3_8)*1.0_8*AB(9_8))+a_in_2(6_8)*2.0_8*AB(4_8))+a_in_3(6_8)*2.0_8*AB(3_8) &
      )+a_in_3(10_8)*1.0_8*AB(1_8))+a_in_4(10_8)*1.0_8
      OUT_INT(27_8)=((a_in_1(3_8)*1.0_8*AB(18_8)+a_in_2(5_8)*3.0_8*AB(8_8))+a_in_3(8_8)*3.0_8*AB(2_8))+a_in_4(12_8)*1.0_8
      OUT_INT(28_8)=((((a_in_1(3_8)*1.0_8*AB(17_8)+a_in_2(5_8)*2.0_8*AB(7_8))+a_in_2(6_8)*1.0_8*AB(8_8))+a_in_3(8_8)*1.0_8*AB(3_8) &
      )+a_in_3(9_8)*2.0_8*AB(2_8))+a_in_4(13_8)*1.0_8
      OUT_INT(29_8)=((((a_in_1(3_8)*1.0_8*AB(16_8)+a_in_2(5_8)*1.0_8*AB(9_8))+a_in_2(6_8)*2.0_8*AB(7_8))+a_in_3(9_8)*2.0_8*AB(3_8) &
      )+a_in_3(10_8)*1.0_8*AB(2_8))+a_in_4(14_8)*1.0_8
      OUT_INT(30_8)=((a_in_1(3_8)*1.0_8*AB(19_8)+a_in_2(6_8)*3.0_8*AB(9_8))+a_in_3(10_8)*3.0_8*AB(3_8))+a_in_4(15_8)*1.0_8
!
   end subroutine hrr_cart_3_1_A_r
!
end module hrr_cart_3_1_A

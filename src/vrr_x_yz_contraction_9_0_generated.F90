module vrr_x_yz_contraction_9_0
!
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!
!
   implicit none
!
!
!
contains
!
!
   subroutine vrr_x_yz_contraction_9_0_s_9_0_r(UNCONTRACTED_INTEGRAL,AUXILIARY_INTEGRAL,BOYS_FUNCTION,CONTRACTED_X_BOYS)
!!
!!    Written by Andrea Bianchi
!!    Autogenerated by the cpint tool
!!    393
      use vrr_contract_x_yz_1_0_module, only: vrr_contract_x_yz_7_0_r
      use vrr_contract_x_yz_1_0_module, only: vrr_contract_x_yz_8_0_r
      use vrr_contract_x_yz_1_0_module, only: vrr_contract_x_yz_9_0_r
!!
      implicit none
!
      real(8), dimension(*), intent(out) :: UNCONTRACTED_INTEGRAL
      real(8), dimension(24463), intent(in) :: AUXILIARY_INTEGRAL
      real(8), dimension(*), intent(in) :: BOYS_FUNCTION
      real(8), dimension(*), intent(in) :: CONTRACTED_X_BOYS
      integer(8) :: I_E
!
      UNCONTRACTED_INTEGRAL(1_8:1_8)=sum(AUXILIARY_INTEGRAL(24354_8:24363_8)*BOYS_FUNCTION(1_8:10_8))
      do I_E = 1_8, 2_8
         UNCONTRACTED_INTEGRAL(((2_8+I_E)-1_8))=sum(AUXILIARY_INTEGRAL(((4_8+((I_E*2_8-2_8)+1_8))-1_8):((4_8+I_E*2_8)-1_8))* &
         CONTRACTED_X_BOYS(918_8:919_8))
      enddo
      do I_E = 1_8, 3_8
         UNCONTRACTED_INTEGRAL(((4_8+I_E)-1_8))=sum(AUXILIARY_INTEGRAL(((32_8+((I_E*3_8-3_8)+1_8))-1_8):((32_8+I_E*3_8)-1_8))* &
         CONTRACTED_X_BOYS(726_8:728_8))
      enddo
      do I_E = 1_8, 4_8
         UNCONTRACTED_INTEGRAL(((7_8+I_E)-1_8))=sum(AUXILIARY_INTEGRAL(((163_8+((I_E*4_8-4_8)+1_8))-1_8):((163_8+I_E*4_8)-1_8))* &
         CONTRACTED_X_BOYS(551_8:554_8))
      enddo
      do I_E = 1_8, 5_8
         UNCONTRACTED_INTEGRAL(((11_8+I_E)-1_8))=sum(AUXILIARY_INTEGRAL(((569_8+((I_E*5_8-5_8)+1_8))-1_8):((569_8+I_E*5_8)-1_8))* &
         CONTRACTED_X_BOYS(395_8:399_8))
      enddo
      do I_E = 1_8, 6_8
         UNCONTRACTED_INTEGRAL(((16_8+I_E)-1_8))=sum(AUXILIARY_INTEGRAL(((1556_8+((I_E*6_8-6_8)+1_8))-1_8):((1556_8+I_E*6_8)-1_8)) &
         *CONTRACTED_X_BOYS(260_8:265_8))
      enddo
      do I_E = 1_8, 7_8
         UNCONTRACTED_INTEGRAL(((22_8+I_E)-1_8))=sum(AUXILIARY_INTEGRAL(((3604_8+((I_E*7_8-7_8)+1_8))-1_8):((3604_8+I_E*7_8)-1_8)) &
         *CONTRACTED_X_BOYS(148_8:154_8))
      enddo
      call vrr_contract_x_yz_7_0_r(UNCONTRACTED_INTEGRAL(29_8:36_8),AUXILIARY_INTEGRAL(7407_8:7470_8),CONTRACTED_X_BOYS(61_8:68_8) &
      )
      call vrr_contract_x_yz_8_0_r(UNCONTRACTED_INTEGRAL(37_8:45_8),AUXILIARY_INTEGRAL(13913_8:13993_8),CONTRACTED_X_BOYS(1_8:9_8) &
      )
      call vrr_contract_x_yz_9_0_r(UNCONTRACTED_INTEGRAL(46_8:55_8),AUXILIARY_INTEGRAL(24364_8:24463_8),BOYS_FUNCTION)
!
   end subroutine vrr_x_yz_contraction_9_0_s_9_0_r
!
end module vrr_x_yz_contraction_9_0

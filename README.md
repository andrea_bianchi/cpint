# cpint

CPINT is an open-source integral library for the calculation of Gaussian integrals in computational chemistry.

## Disclaimer

This repository contains only the automatically generated code from the cpint integral library, which is currently in the development phase. This code enables the computation of the most common integrals in computational chemistry and showcases the structure of the interface until g Gaussian type functions.

## Prerequisites 

To use this library, ensure the following prerequisites are met:

CMake (version 3.7 or newer)
gfortran (currently the only supported compiler)

## How to install 

To install cpint, follow these steps:

Clone the repository:
```
git clone https://gitlab.com/andrea_bianchi/cpint.git
cd cpint
```
Create a build directory:
```
mkdir build
cd build
```
Run cmake:
```
cmake .. -DCMAKE_INSTALL_PREFIX=/where/you/want/to/install/
```
Install:
```
cmake --build . -j[number of cores] --target install
```

## Integral included in this release
The integrals included in this release are:

One electron integrals:

|         integral       |         name           |
|------------------------|------------------------|
| ( A \|  B)             | get_shell_overlap      |
| (∇A \|  B)             | get_shell_overlap_ip_A |
| ( A \| ∇B)             | get_shell_overlap_ip_B |
| ( A \| r \| B)         | get_shell_dipole       |
| ( A \| r \| ∇B)        | get_shell_dipole_ip_B  |
| ( A \| r r \| B)       | get_shell_quadrupole   |
| ( A \| ∇ ⋅ ∇ \| B)     | get_shell_kinetic      |
| (∇A \| ∇ ⋅ ∇ \| B)     | get_shell_kinetic_ip_A |
| ( A \| r × ∇ \| B)     | get_shell_r_cross_ip   |
| (∇A \| 1/(r - C) \| B) | get_shell_ep_ip_A      |
| ( A \| 1/(r - C) \| B) | get_shell_ep_module    |

Two electron integrals:

|             integral            |        name        |
|---------------------------------|--------------------|
| ( A  B \| 1 / (r_12) \|  C  D ) | get_shell_eri      |
| (∇A  B \| 1 / (r_12) \|  C  D ) | get_shell_eri_ip_A |
| ( A ∇B \| 1 / (r_12) \|  C  D ) | get_shell_eri_ip_B |
| ( A  B \| 1 / (r_12) \| ∇C  D ) | get_shell_eri_ip_C |
| ( A  B \| 1 / (r_12) \|  C ∇D ) | get_shell_eri_ip_D |

# How to call and integral 
To use an integral, follow these steps:

it is recommended to include the cpint interface:
```
   interface 
!
        include "cpint.F90"
!
   end interface
```
To call one-electon integrals:
```
call [integral_name]([result], 
                     [angmom of the shell A],
                     [number of primitives in the shell A],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell A],
                     [coordinates of the shell A],
                     [bool that defines if A is pure], 
                     [angmom of the shell B],
                     [number of primitives in the shell B],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell B],
                     [coordinates of the shell B],
                     [bool that defines if B is pure], 
                     [precision threshold], 
                     [prefactor], 
                     [bool that returns if the integral is zero])
```
For electrostatic potential integrals (1/(r-C) where C represents the charge center):
```
call [integral_name]([result], 
                     [angmom of the shell A],
                     [number of primitives in the shell A],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell A],
                     [coordinates of the shell A],
                     [bool that defines if A is pure], 
                     [angmom of the shell B],
                     [number of primitives in the shell B],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell B],
                     [coordinates of the shell B],
                     [bool that defines if B is pure], 
                     [precision threshold], 
                     [prefactor], 
                     [bool that returns if the integral is zero], 
                     [integer with the number of the charges],
                     [array this the values of the charges],
                     [array this the coordinates of the charges])
```
For two-electons integrals:

```
call [integral_name]([result], 
                     [angmom of the shell A],
                     [number of primitives in the shell A],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell A],
                     [coordinates of the shell A],
                     [bool that defines if A is pure], 
                     [angmom of the shell B],
                     [number of primitives in the shell B],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell B],
                     [coordinates of the shell B],
                     [bool that defines if B is pure], 
                     [angmom of the shell C],
                     [number of primitives in the shell C],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell C],
                     [coordinates of the shell C],
                     [bool that defines if C is pure], 
                     [angmom of the shell D],
                     [number of primitives in the shell D],
                     [2D array of the coefficients (first index) and exponents (second index) of the shell D],
                     [coordinates of the shell D],
                     [bool that defines if D is pure], 
                     [precision threshold], 
                     [prefactor], 
                     [bool that returns if the integral is zero])
```
